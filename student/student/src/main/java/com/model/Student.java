package com.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	@Id@GeneratedValue
	private int studentId;
	@Column(name="sname")
	private String studentName;
	private String Branch;
	
	public Student() {
		super();
	}

	public Student(int studentId, String studentName, String branch) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.Branch = branch;
	}


	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getBranch() {
		return Branch;
	}

	public void setBranch(String branch) {
		this.Branch = branch;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", Branch=" + Branch + "]";
	}

}